#!/bin/bash
if [ -n "$ENABLE_BACKLIGHT_TIMER" ]
then
  (crontab -l; echo "${BACKLIGHT_ON:-0 8 * * *} /usr/src/backlight_on.sh") | crontab -
  (crontab -l; echo "${BACKLIGHT_OFF:-0 23 * * *} /usr/src/backlight_off.sh") | crontab -
fi

if [ -n "$ENABLE_MONITOR_TIMER" ]
then
  (crontab -l; echo "${MONITOR_ON:-0 8 * * *} /usr/src/monitor_on.sh") | crontab -
  (crontab -l; echo "${MONITOR_OFF:-0 23 * * *} /usr/src/monitor_off.sh") | crontab -
fi

if [ -n "$TIMEZONE" ]
then
  echo "${TIMEZONE}" > /etc/timezone
  cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime
fi

crond -f
